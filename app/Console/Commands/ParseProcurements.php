<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;


use App\Procurement;
use App\Setting;


class ParseProcurements extends Command {
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'procurements:parse';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Change Procurement\'s statuses depending on their period of publication and global settings';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $settings = Setting::find(1);
        $curDate = date('Y-m-d H:i:s');


        // автоматическая публикация закупок со статусом "черновик" при наступлении срока подачи заявок
        if ('automatic' == $settings->publishing) {
            $procurements = Procurement::where('status', '=', 'draft')
                ->where('dateStart', '!=', '')
                ->where('dateEnd', '!=', '')
                ->get();

            foreach ($procurements as $procurement) {
                if ($curDate >= $procurement->dateStart and $curDate <= $procurement->dateEnd) {
                    $procurement->update(['status' => 'actual']);
                }
            }
        }


        // автоматическое снятие закупок с публикации по истечении срока подачи заявок
        if ('automatic' == $settings->depublishing) {
            $procurements = Procurement::where('status', '=', 'actual')
                ->where('dateStart', '!=', '')
                ->where('dateEnd', '!=', '')
                ->get();

            foreach ($procurements as $procurement) {
                if ($curDate >= $procurement->dateEnd) {
                    if ('archive' == $settings->removal) {
                        $procurement->update(['status' => 'archive']);
                    } else {
                        Storage::deleteDirectory('public/' . $procurement->id);
                        $procurement->delete();
                    }
                }
            }
        }


    }




}
