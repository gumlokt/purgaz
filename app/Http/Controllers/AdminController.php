<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Procurement;
use App\User;



class AdminController extends Controller {
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $statistics['actual'] = Procurement::where('status', '=', 'actual')->count();
        $statistics['draft'] = Procurement::where('status', '=', 'draft')->count();
        $statistics['archive'] = Procurement::where('status', '=', 'archive')->count();

        $procurements = Procurement::where('status', '=', 'actual')
            ->where(function ($query) {
                $query->where('announcement', '=', false)
                    ->orWhere('documentation', '=', false);
                })
            ->get();


        $statistics['incomplete'] = count($procurements);



        return view('adminpanel.index', ['statistics' => $statistics, 'procurements' => $procurements]);
    }



    // show procurements by status
    public function show($status) {
        $title = $this->statuses[$status];
        $procurements = Procurement::where('status', '=', $status)
            ->orderBy('created_at', 'DESC')
            ->get();

        return view('adminpanel.sections.procurements', ['title' => $title, 'procurements' => $procurements]);
    }






}
