<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;


use App\Procurement;
use App\Setting;


class FileController extends Controller {


    // Работа с файлами - скачивание/удаление
    public function file($type, $id) {
        $settings = Setting::find(1);
        $procurement = Procurement::find($id);

        if ('draft' == $procurement->status) {
            return  back();
        }

        if ('archive' == $procurement->status and 'hide' == $settings->archives) {
            return  back();
        }



        $link = '';
        $pattern = '/' . $type . '/';

        $directoryContent = Storage::files('public/' . $id);
        foreach ($directoryContent as $file) {
            if (preg_match($pattern, $file)) {
                $link = $file;
            }
        }


        return response()->download(storage_path('app/') . $link);
    }






}
