<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Mail;


use App\Mail\FeedbackSender;

use App\Procurement;
use App\Setting;


class IndexController extends Controller {

    // default action
    public function section($section = null) {
        if ($section) {
            return view('sections.' . $section);
        }


        return view('index');
    }



    // display procurements
    public function procurements() {
        $settings = Setting::find(1);


        $procurements = Procurement::where('status', '=', 'actual')
            ->orderBy('created_at', 'DESC')
            ->get();

        if ('show' == $settings->archives) {
            $archives = Procurement::where('status', '=', 'archive')
                ->orderBy('created_at', 'DESC')
                ->get();

            return view('sections.procurements', ['settings' => $settings, 'procurements' => $procurements, 'archives' => $archives]);
        }



        return view('sections.procurements', ['settings' => $settings, 'procurements' => $procurements]);
    }



    // contacts
    public function contacts(Request $request) {

        if ($request->isMethod('get')) {
            return view('sections.contacts');
        }

        $this->validate($request, [
            'name' => 'required|max:255',
            'email' => 'required|email',
            'subject' => 'required|max:255',
            'message' => 'required',
        ]);


        Mail::to('zaopurgaz@purgaz.com')->send(new FeedbackSender);
        Mail::to('zaopurgaz@yandex.ru')->send(new FeedbackSender);


        return back()->with('message', 'Ваше сообщение успешно отправлено...');
    }







}
