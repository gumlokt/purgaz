<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use App\Procurement;
use Auth;


class ProcurementController extends Controller {
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('adminpanel.sections.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $sql = [
            'creator_id'      => Auth::user()->id,
            'updater_id'      => Auth::user()->id,
            'status'          => $request->input('status'),
            'number'          => null == $request->input('number') ? '' : $request->input('number'),
            'title'           => null == $request->input('title') ? '' : $request->input('title'),
            'comment'         => null == $request->input('comment') ? '' : $request->input('comment'),

            'announcement'    => $request->hasFile('announcement') ? true : false,
            'documentation'   => $request->hasFile('documentation') ? true : false,

            'dateStart'       => $request->input('dateStart'),
            'dateEnd'         => $request->input('dateEnd'),
        ];

        $procurement = Procurement::create($sql);




        if ($request->hasFile('announcement')) {
            $request->file('announcement')->storeAs('public/' . $procurement->id, 'purgaz_announcement_' . $procurement->number . '.' . $request->file('announcement')->extension());
        }

        if ($request->hasFile('documentation')) {
            $request->file('documentation')->storeAs('public/' . $procurement->id, 'purgaz_documentation_' . $procurement->number . '.' . $request->file('documentation')->extension());
        }



        return redirect()->route('procurements', ['title' => $request->input('status'), ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $procurement = Procurement::find($id);

        $files = [
            'announcement'  => false,
            'documentation' => false
        ];

        $directoryContent = Storage::files('public/' . $procurement->id);
        foreach ($directoryContent as $file) {
            if (preg_match('/announcement/', $file)) {
                $files['announcement'] = $file;
            }
            if (preg_match('/documentation/', $file)) {
                $files['documentation'] = $file;
            }
        }




        return view('adminpanel.sections.create', ['files' => $files, 'procurement' => $procurement]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $procurement = Procurement::find($id);

        $sql = [
            'updater_id'      => Auth::user()->id,
            'status'          => $request->input('status'),
            'number'          => null == $request->input('number') ? '' : $request->input('number'),
            'title'           => null == $request->input('title') ? '' : $request->input('title'),
            'comment'         => null == $request->input('comment') ? '' : $request->input('comment'),

            'announcement'    => $procurement->announcement ? true : $request->hasFile('announcement') ? true : false,
            'documentation'    => $procurement->documentation ? true : $request->hasFile('documentation') ? true : false,
            // 'announcement'    => $request->hasFile('announcement') ? true : false,
            // 'documentation'   => $request->hasFile('documentation') ? true : false,

            'dateStart'       => $request->input('dateStart'),
            'dateEnd'         => $request->input('dateEnd'),
        ];

        $procurement->update($sql);


        if ($request->hasFile('announcement')) {
            $request->file('announcement')->storeAs('public/' . $procurement->id, 'purgaz_announcement_' . $procurement->number . '.' . $request->file('announcement')->extension());
        }

        if ($request->hasFile('documentation')) {
            $request->file('documentation')->storeAs('public/' . $procurement->id, 'purgaz_documentation_' . $procurement->number . '.' . $request->file('documentation')->extension());
        }



        return redirect()->route('procurements', ['title' => $request->input('status'), ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id) {
        $procurement = Procurement::find($id);

        if ($request->isMethod('get')) {
            return view('adminpanel.sections.confirmation', ['procurement' => $procurement]);
        }

        $procurement->destroy($id);
        Storage::deleteDirectory('public/' . $id);

        return redirect()->route('procurements', ['title' => $procurement->status, ]);
    }




    public function destroyAll(Request $request) {
        $procurements = Procurement::where('status', '=', 'archive')->get();


        if ($request->isMethod('get')) {
            return view('adminpanel.sections.confirmation', ['procurements' => $procurements]);
        }

        foreach ($procurements as $procurement) {
            Storage::deleteDirectory('public/' . $procurement->id);
            $procurement->delete();
        }


        return redirect()->route('procurements', 'archive');
    }









    // Activate procurement if it paused
    public function activate($id) {
        $procurement = Procurement::where('id', $id)->update(['updater_id' => Auth::user()->id, 'status' => 'actual']);

        return back();
        // return redirect()->route('procurements', ['title' => 'actual', ]);
    }

    // Mark procurement as draft (pause it)
    public function draft($id) {
        $procurement = Procurement::where('id', $id)->update(['updater_id' => Auth::user()->id, 'status' => 'draft']);

        return back();
        // return redirect()->route('procurements', ['title' => 'draft', ]);
    }

    // Mark procurement as archive (move to trash)
    public function archive($id) {
        $procurement = Procurement::where('id', $id)->update(['updater_id' => Auth::user()->id, 'status' => 'archive']);

        return back();
        // return redirect()->route('procurements', ['title' => 'archive', ]);
    }














    // Работа с файлами - скачивание/удаление
    public function file($action, $type, $id) {
        $link = '';
        $pattern = '/' . $type . '/';

        $directoryContent = Storage::files('public/' . $id);
        foreach ($directoryContent as $file) {
            if (preg_match($pattern, $file)) {
                $link = $file;
            }
        }


        // if download action is requested
        if('download' == $action) {
            return response()->download(storage_path('app/') . $link);
        }



        // if delete action is requested
        // delete requested file
        Storage::delete($link);

        // delete directory if it became empty
        if (count($directoryContent) < 2) {
            Storage::deleteDirectory('public/' . $id);
        }


        $procurement = Procurement::find($id);


        if('announcement' == $type) {
            $procurement->update(['announcement' => false]);
            return redirect('/adminpanel/procurment/' . $id . '/edit')->with('announcement', 'файл успешно удалён');
        }

        $procurement->update(['documentation' => false]);
        return redirect('/adminpanel/procurment/' . $id . '/edit')->with('documentation', 'файл успешно удалён');
    }





}

