<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Setting;
use Auth;

class SettingController extends Controller {
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }


    public function show() {
        $settings = Setting::find(1);

        return view('adminpanel.sections.settings', ['settings' => $settings]);
    }


    public function save(Request $request) {
        $sql = [
            'updater_id'    => Auth::user()->id,
            'publishing'    => $request->input('publishing'),
            'depublishing'  => $request->input('depublishing'),
            'removal'       => $request->input('removal'),
            'archives'      => $request->input('archives'),
            'comments'      => $request->input('comments'),
        ];

        $settings = Setting::find(1);
        $settings->update($sql);


        return view('adminpanel.sections.notification', ['updating' => true, ]);
    }



    public function cancel(Request $request) {
        return view('adminpanel.sections.notification', ['updating' => false, ]);
    }




}
