<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use Illuminate\Http\Request;

class FeedbackSender extends Mailable {
    use Queueable, SerializesModels;

    /**
     * The order instance.
     *
     * @var Order
     */
    public $message;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct() {
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(Request $request) {


        return $this->from('zaopurgaz@yandex.ru')
            ->subject('[www.purgaz.com]')
            ->view('emails.feedback', ['request' => $request]);
    }





}
