<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Procurement extends Model {
    protected $guarded = ['id', 'created_at', 'updated_at', ];

    public function creators() {
        return $this->belongsTo('App\User', 'creator_id');
    }

    public function updaters() {
        return $this->belongsTo('App\User', 'updater_id');
    }




}
