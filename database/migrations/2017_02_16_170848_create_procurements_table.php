<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProcurementsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('procurements', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('creator_id')->default(1);
            $table->integer('updater_id')->default(1);

            $table->enum('status', ['actual', 'draft', 'archive']);
            $table->string('number', 100);
            $table->text('title');
            $table->text('comment');

            $table->boolean('announcement')->default(0);
            $table->boolean('documentation')->default(0);

            $table->timestamp('dateStart')->nullable();
            $table->timestamp('dateEnd')->nullable();


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('procurements');
    }
}
