<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('settings', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('updater_id')->default(1);

            $table->enum('publishing', ['automatic', 'manual', ])->default('automatic');
            $table->enum('depublishing', ['automatic', 'manual', ])->default('automatic');
            $table->enum('removal', ['archive', 'destroy', ])->default('archive');
            $table->enum('archives', ['hide', 'show', ])->default('hide');
            $table->enum('comments', ['hide', 'show', ])->default('hide');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('settings');
    }
}
