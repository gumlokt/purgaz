<?php

use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        DB::table('settings')->insert([
            'publishing' => 'automatic',
            'depublishing' => 'automatic',
            'removal' => 'archive',
            'archives' => 'hide',
            'comments' => 'hide',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);
    }
}
