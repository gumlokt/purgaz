$(document).ready(function() {

    $('.fa-question-circle-o').tooltip();


    $.datetimepicker.setLocale('ru');

    $('#dateStart').datetimepicker({
        format:         'Y-m-d H:i',
        defaultTime:    '08:15',
        step:           15
    });

    $('#dateEnd').datetimepicker({
        format:         'Y-m-d H:i',
        defaultDate:    '+1970/01/10',
        defaultTime:    '11:00',
        step:           15
    });



    $('#autoDepublishing').click(function() {
        $("#removal").show('slow');
        $("#removal").next().show('slow');
    });

    $('#manualDepublishing').click(function() {
        $("#removal").next().hide('slow');
        $("#removal").hide('slow');
    });


});

