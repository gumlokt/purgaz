    <footer class="footer">
        <div class="container">
            <div class="row">
                <div class="col">
                    <span class="text-muted">&copy; 2014-{{ date('Y') }} <span class="text-danger">ЗАО «Пургаз»</span></span>
                </div>

                <div class="col-right">
                    @if (Auth::guest())
                        <a href="{{ route('login') }}"><i class="fa fa-sign-in" aria-hidden="true"></i> Вход</a>
                    @else
                        <a href="{{ url('/') }}"><i class="fa fa-home text-danger" aria-hidden="true"></i> На главную</a> | 
                        Вы вошли как: <strong class="text-danger">{{ Auth::user()->name }}</strong> | 
                        <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="fa fa-sign-out text-danger" aria-hidden="true"></i> Выход</a>
                    @endif
                </div>
            </div>
        </div>
    </footer>
