@extends('adminpanel.layouts.app')

@section('content')
    <div class="row">
        <div class="col">


            <div class="card">
                <div class="card-header">
                    <h5 class="text-danger">Общие сведения web-приложения</h5>
                </div>

                <div class="card-block">
                    <div class="card-group">
                        <div class="card">
                            <div class="card-block text-center">
                                <h5 class="card-title text-success"><i class="fa fa-circle" aria-hidden="true"></i> Опубликованные</h5>
                                <hr>
                                <a class="btn btn-outline-success btn-lg btn-block" href="{{ url('adminpanel/procurments/actual') }}">
                                    {{ $statistics['actual'] }}
                                </a>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-block text-center">
                                <h5 class="card-title text-warning"><i class="fa fa-pause" aria-hidden="true"></i> Черновики</h5>
                                <hr>
                                <a class="btn btn-outline-warning btn-lg btn-block" href="{{ url('adminpanel/procurments/draft') }}">
                                    {{ $statistics['draft'] }}
                                </a>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-block text-center">
                                <h5 class="card-title text-primary"><i class="fa fa-trash" aria-hidden="true"></i> Архивы</h5>
                                <hr>
                                <a class="btn btn-outline-primary btn-lg btn-block" href="{{ url('adminpanel/procurments/archive') }}">
                                    {{ $statistics['archive'] }}
                                </a>
                            </div>
                        </div>
                    </div>
                    <br>

                    <div class="card-group">
                        <div class="card">
                            <div class="card-block">
                                <h5 class="card-title">
                                    Текущее время на сервере: <span class="text-danger">{{ date('Y-m-d H:i:s') }}</span>
                                    <small class="text-muted">[ <em>формат времени</em>: <span class="text-info">ГГГГ-ММ-ДД ЧЧ:ММ:СС</span> ]</small>
                                </h5>
                            </div>
                        </div>
                    </div>
                    <br>

                    @if (isset($procurements) and !empty($procurements[0]))
                        <div class="card-group">
                            <div class="card">
                                <div class="card-block">
                                    <h5 class="card-title"><i class="fa fa-meh-o text-danger" aria-hidden="true"></i> Закупки из раздела <strong>"Опубликованные"</strong> без приложенных файлов: <span class="badge badge-pill badge-danger">{{ $statistics['incomplete'] }}</span></h5>

                                    @include('adminpanel.includes.table')
                                </div>
                            </div>
                        </div>
                        <br>
                    @endif




                    <div class="header">
                    </div>
                </div>
            </div>


        </div>
    </div>

@endsection
