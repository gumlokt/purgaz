<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" href="{{ url('ico/favicon.png') }}">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Пургаз</title>


    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ url('assets/bootstrap-4.0.0-alpha.6-dist/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ url('assets/font-awesome-4.7.0/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ url('assets/datetimepicker-master/jquery.datetimepicker.css') }}">

    <!-- Styles -->
    <link href="{{ url('css/main.css') }}" rel="stylesheet">

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
</head>
<body>

    <div class="container" style="margin-bottom: 1em;">
        <div class="row">
            <div class="col-4">
                <a class="navbar-brand" href="{{ url('/') }}">
                    <img src="{{ url('images/logo.png') }}" alt="logo" id="logo">
                </a>
            </div>
            <div class="col-8">
          <span class="pull-right" style="text-shadow: 2px  2px  5px #696969, 0 0 1px #0093dd; color: white; font-size: 28px; border: 0px solid blue;">
            <strong>Закрытое акционерное общество &laquo;Пургаз&raquo;</strong>
          </span>
            </div>
        </div>
    </div>


    <div class="container">
        @include('adminpanel.navbar')

        @yield('content')
    </div>


    @include('adminpanel.footer')









    <!-- jQuery first, then Tether, then Bootstrap JS. -->
    <script src="{{ url('assets/jquery-3.1.1.min.js') }}"></script>
    <script src="{{ url('assets/tether-master/dist/js/tether.min.js') }}"></script>
    <script src="{{ url('assets/bootstrap-4.0.0-alpha.6-dist/js/bootstrap.min.js') }}"></script>
    <script src="{{ url('assets/datetimepicker-master/build/jquery.datetimepicker.full.min.js') }}"></script>

    <!-- Custom JS -->
    <script src="{{ url('js/purgaz.js') }}"></script>
    <script src="{{ url('js/adminpanel.js') }}"></script>


</body>
</html>
