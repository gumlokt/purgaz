    {{-- <nav class="navbar navbar-inverse bg-inverse navbar-toggleable-md"> --}}
    {{-- <nav class="navbar navbar-light bg-faded rounded navbar-toggleable-md"> --}}
    <nav class="navbar navbar-light bg-faded rounded navbar-toggleable-md" style="background-color: #e3f2fd;">
        <div class="container">
            <button class="navbar-toggler navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleContainer" aria-controls="navbarsExampleContainer" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <a class="navbar-brand" href="{{ url('adminpanel') }}"><i class="fa fa-bar-chart" aria-hidden="true"></i> Статистика</a>


            <div class="collapse navbar-collapse" id="navbarsExampleContainer">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item{{ ends_with(url()->current(), url('adminpanel/procurment/create')) ? ' active' : '' }}"><a class="nav-link" href="{{ url('adminpanel/procurment/create') }}"><i class="fa fa-file-o" aria-hidden="true"></i> Создать</a></li>
                </ul>

                <ul class="navbar-nav mr-auto">
                    <li class="nav-item"><a class="nav-link" href="#" style="cursor: default;">Закупки:</a></li>
                    <li class="nav-item{{ ends_with(url()->current(), url('adminpanel/procurments/actual')) ? ' active' : '' }}"><a class="nav-link" href="{{ url('adminpanel/procurments/actual') }}"><i class="fa fa-circle text-success" aria-hidden="true"></i> Опубликованные</a></li>
                    <li class="nav-item{{ ends_with(url()->current(), url('adminpanel/procurments/draft')) ? ' active' : '' }}"><a class="nav-link" href="{{ url('adminpanel/procurments/draft') }}"><i class="fa fa-pause text-warning" aria-hidden="true"></i> Черновики</a></li>
                    <li class="nav-item{{ ends_with(url()->current(), url('adminpanel/procurments/archive')) ? ' active' : '' }}"><a class="nav-link" href="{{ url('adminpanel/procurments/archive') }}"><i class="fa fa-trash text-primary" aria-hidden="true"></i> Архивы</a></li>
                </ul>

                @if (!Auth::guest())
                    <ul class="navbar-nav justify-content-end">
                        <li class="nav-item{{ ends_with(url()->current(), url('adminpanel/settings/show')) ? ' active' : '' }}"><a class="nav-link" href="{{ url('adminpanel/settings/show') }}"><i class="fa fa-sliders" aria-hidden="true"></i> Настройки</a></li>
                        <li class="nav-item"><a class="nav-link text-danger" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="fa fa-sign-out" aria-hidden="true"></i> Выход</a></li>
                    </ul>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                @endif
            </div>
        </div>
    </nav>
