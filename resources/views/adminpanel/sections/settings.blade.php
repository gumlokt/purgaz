@extends('adminpanel.layouts.app')

@section('content')
    <div class="row">
        <div class="col">

            <div class="card">
                <div class="card-header">
                    <h5 class="text-danger">Настройки web-приложения</h5>
                </div>

                <div class="card-block">
                    <form action="{{ url('adminpanel/settings/save') }}" method="POST">
                        {{ csrf_field() }}




                        <div class="form-group row">
                            <div class="col-sm-12">
                                <h5 class="text-muted">Настройки публикации</h5>
                            </div>
                        </div>
                        <hr>

                        <div class="form-group row">
                            <div class="col-sm-7">
                                <i class="fa fa-question-circle-o fa-lg text-muted" aria-hidden="true" data-toggle="tooltip" data-placement="right" data-html="true" title="Режим публикации закупок из раздела <strong>Черновики</strong>"></i> Режим публикации закупок
                            </div>
                            <div class="col-sm-3">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="radio" name="publishing" value="automatic"{{ isset($settings) ? 'automatic' == $settings->publishing ? ' checked' : '' : '' }}> автоматический <i class="fa fa-question-circle-o text-muted" aria-hidden="true" data-toggle="tooltip" data-placement="right" data-html="true" title="Закупки, находящиеся в разделе <strong>Черновики</strong> будут публиковаться автоматически при наступлении периода подачи заявок"></i>
                                </label>
                            </div>
                            <div class="col-sm-2">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="radio" name="publishing" value="manual"{{ isset($settings) ? 'manual' == $settings->publishing ? ' checked' : '' : '' }}> ручной <i class="fa fa-question-circle-o text-muted" aria-hidden="true" data-toggle="tooltip" data-placement="right" data-html="true" title="Закупки, находящиеся в разделе <strong>Черновики</strong> будут публиковаться вручную"></i>
                                </label>
                            </div>
                        </div>
                        <hr>

                        <div class="form-group row">
                            <div class="col-sm-7">
                                <i class="fa fa-question-circle-o fa-lg text-muted" aria-hidden="true" data-toggle="tooltip" data-placement="right" data-html="true" title="Режим снятия закупок с публикации из раздела <strong>Опубликованные</strong>"></i> Режим снятия закупок с публикации
                            </div>
                            <div class="col-sm-3">
                                <label class="form-check-label" id="autoDepublishing">
                                    <input class="form-check-input" type="radio" name="depublishing" value="automatic"{{ isset($settings) ? 'automatic' == $settings->depublishing ? ' checked' : '' : '' }}> автоматический <i class="fa fa-question-circle-o text-muted" aria-hidden="true" data-toggle="tooltip" data-placement="right" data-html="true" title="Закупки, находящиеся в разделе <strong>Опубликованные</strong> будут автоматически сниматься с публикации при истечении периода подачи заявок"></i>
                                </label>
                            </div>
                            <div class="col-sm-2">
                                <label class="form-check-label" id="manualDepublishing">
                                    <input class="form-check-input" type="radio" name="depublishing" value="manual"{{ isset($settings) ? 'manual' == $settings->depublishing ? ' checked' : '' : '' }}> ручной <i class="fa fa-question-circle-o text-muted" aria-hidden="true" data-toggle="tooltip" data-placement="right" data-html="true" title="Закупки, находящиеся в разделе <strong>Опубликованные</strong> будут сниматься с публикации вручную"></i>
                                </label>
                            </div>
                        </div>
                        <hr>

                        <div class="form-group row" id="removal" style="{{ isset($settings) ? 'manual' == $settings->depublishing ? 'display: none;' : '' : '' }}">
                            <div class="col-sm-7">
                                &emsp;&emsp;<i class="fa fa-question-circle-o fa-lg text-muted" aria-hidden="true" data-toggle="tooltip" data-placement="right" data-html="true" title="Действие, применяемое к закупкам из раздела <strong>Опубликованные</strong> при истечении срока подачи заявок и включенном автоматическом режиме снятия закупок с публикации"></i> Действие при автоматическом снятии закупок с публикации
                            </div>
                            <div class="col-sm-3">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="radio" name="removal" value="archive"{{ isset($settings) ? 'archive' == $settings->removal ? ' checked' : '' : '' }}> архивация <i class="fa fa-question-circle-o text-muted" aria-hidden="true" data-toggle="tooltip" data-placement="right" data-html="true" title="Закупки будут перемещаться в  раздел <strong>Архивы</strong>"></i>
                                </label>
                            </div>
                            <div class="col-sm-2">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="radio" name="removal" value="destroy"{{ isset($settings) ? 'destroy' == $settings->removal ? ' checked' : '' : '' }}> удаление <i class="fa fa-question-circle-o text-muted" aria-hidden="true" data-toggle="tooltip" data-placement="right" data-html="true" title="Закупки будут удаляться (без возможности восстановления)"></i>
                                </label>
                            </div>
                        </div>
                        <hr style="{{ isset($settings) ? 'manual' == $settings->depublishing ? 'display: none;' : '' : '' }}"><br>




                        <div class="form-group row">
                            <div class="col-sm-12">
                                <h5 class="text-muted">Настройки отображения</h5>
                            </div>
                        </div>
                        <hr>

                        <div class="form-group row">
                            <div class="col-sm-7">
                                <i class="fa fa-question-circle-o fa-lg text-muted" aria-hidden="true" data-toggle="tooltip" data-placement="right" data-html="true" title="Настройка отображения закупок из раздела <strong>Архивы</strong>"></i> Архивные закупки
                            </div>
                            <div class="col-sm-3">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="radio" name="archives" value="hide"{{ isset($settings) ? 'hide' == $settings->archives ? ' checked' : '' : '' }}> скрывать <i class="fa fa-question-circle-o text-muted" aria-hidden="true" data-toggle="tooltip" data-placement="right" data-html="true" title="Скрывать от посетителей сайта закупки из раздела <strong>Архивы</strong>"></i>
                                </label>
                            </div>
                            <div class="col-sm-2">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="radio" name="archives" value="show"{{ isset($settings) ? 'show' == $settings->archives ? ' checked' : '' : '' }}> отображать <i class="fa fa-question-circle-o text-muted" aria-hidden="true" data-toggle="tooltip" data-placement="right" data-html="true" title="Отображать посетителям сайта закупки из раздела <strong>Архивы</strong>"></i>
                                </label>
                            </div>
                        </div>
                        <hr>

                        <div class="form-group row">
                            <div class="col-sm-7">
                                <i class="fa fa-question-circle-o fa-lg text-muted" aria-hidden="true" data-toggle="tooltip" data-placement="right" data-html="true" title="Настройка отображения поля <strong>Примечание</strong>"></i> Примечания
                            </div>
                            <div class="col-sm-3">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="radio" name="comments" value="hide"{{ isset($settings) ? 'hide' == $settings->comments ? ' checked' : '' : '' }}> скрывать <i class="fa fa-question-circle-o text-muted" aria-hidden="true" data-toggle="tooltip" data-placement="right" data-html="true" title="Скрывать от посетителей сайта примечания к закупкам"></i>
                                </label>
                            </div>
                            <div class="col-sm-2">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="radio" name="comments" value="show"{{ isset($settings) ? 'show' == $settings->comments ? ' checked' : '' : '' }}> отображать <i class="fa fa-question-circle-o text-muted" aria-hidden="true" data-toggle="tooltip" data-placement="right" data-html="true" title="Отображать посетителям сайта примечания к закупкам"></i>
                                </label>
                            </div>
                        </div>
                        <hr>
                        <br><br>





                        <div class="form-group row">
                            <div class="col-sm-12 text-center">
                                <button type="submit" class="btn btn-outline-success"><i class="fa fa-floppy-o" aria-hidden="true"></i> Сохранить</button>
                                <button type="submit" class="btn btn-outline-warning" formaction="{{ url('adminpanel/settings/cancel') }}" formmethod="POST"><i class="fa fa-remove" aria-hidden="true"></i> Отменить</button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>

        </div>
    </div>







@endsection
