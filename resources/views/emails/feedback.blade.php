<strong>Это сообщение отправлено посетителем сайта www.purgaz.com через форму обратной связи раздела "Контакты"</strong>
<br><br>


<table class="table table-hover" width="100%">

    <tr style="border: 1px solid #ccc;">
        <td style="border: 1px solid #ccc;" width='15%'><strong>Имя отправителя:</strong></td>
        <td style="border: 1px solid #ccc;">{{ $request->input('name') }}</td>
    </tr>

    <tr style="border: 1px solid #ccc;">
        <td style="border: 1px solid #ccc;"><strong>E-Mail отправителя:</strong></td>
        <td style="border: 1px solid #ccc;">{{ $request->input('email') }}</td>
    </tr>

    <tr style="border: 1px solid #ccc;">
        <td style="border: 1px solid #ccc;"><strong>Тема:</strong></td>
        <td style="border: 1px solid #ccc;">{{ $request->input('subject') }}</td>
    </tr>

    <tr style="border: 1px solid #ccc;">
        <td style="border: 1px solid #ccc;"><strong>Сообщение:</strong></td>
        <td style="border: 1px solid #ccc;">{{ $request->input('message') }}</td>
    </tr>

</table>
