    <footer class="footer">
        <div class="container">
            <div class="row">
                <div class="col">
                    <span class="text-muted">&copy; 2014-{{ date('Y') }} <span class="text-danger">АО «Пургаз»</span></span>
                </div>

                <div class="col-right">
                    @if (Auth::guest())
                    {{-- <a href="{{ route('register') }}"><i class="fa fa-sign-in" aria-hidden="true"></i> Регистрация</a>

                    <a href="{{ route('login') }}" class="text-danger">
                        <i class="fa fa-sign-in" aria-hidden="true"></i> Вход
                    </a> --}}
                    @else
                    <a href="{{ url('adminpanel') }}"><i class="fa fa-cogs text-danger" aria-hidden="true"></i> Панель управления</a> |
                    Вы вошли как: <strong class="text-danger">{{ Auth::user()->name }}</strong> |
                    <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="fa fa-sign-out text-danger" aria-hidden="true"></i> Выход</a>
                    @endif
                </div>
            </div>
        </div>
    </footer>