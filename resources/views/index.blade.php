@extends('layouts.app')


@section('content')
    <div class="row">
        <div class="col">
            <h3>Главная</h3>
        </div>
    </div>
    <hr>

    <div class="row">
        <div class="col-sm-12">



            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="4"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="5"></li>
                </ol>

                <div class="carousel-inner" role="listbox">
                    <div class="carousel-item active">
                        <img class="d-block img-fluid" src="{{ url('images/slide01.jpg') }}" alt="First slide">
                    </div>
                    <div class="carousel-item">
                        <img class="d-block img-fluid" src="{{ url('images/slide02.jpg') }}" alt="Second slide">
                    </div>
                    <div class="carousel-item">
                        <img class="d-block img-fluid" src="{{ url('images/slide03.jpg') }}" alt="Third slide">
                    </div>
                    <div class="carousel-item">
                        <img class="d-block img-fluid" src="{{ url('images/slide04.jpg') }}" alt="Fourth slide">
                    </div>
                    <div class="carousel-item">
                        <img class="d-block img-fluid" src="{{ url('images/slide05.jpg') }}" alt="Fifth slide">
                    </div>
                    <div class="carousel-item">
                        <img class="d-block img-fluid" src="{{ url('images/slide06.jpg') }}" alt="Sixth slide">
                    </div>
                </div>

                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>

                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>

        </div>
    </div>

    <div class="row" style="margin-top: 20px;">
        <div class="col">
            <p class="text-justify">
                Закрытое акционерное общество «Пургаз» было образовано в 1998 году в городе Губкинский, как совместное предприятие ЗАО «ТЭК «Итера-Русь», входящее в группу компаний «Итера» и ООО «Ноябрьскгаздобыча»...
            </p>

            <a href="{{ url('about') }}" type="button" class="btn btn-primary">Подробнее »</a>
        </div>
    </div>
@endsection
