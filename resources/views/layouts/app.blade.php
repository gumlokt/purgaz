<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" href="{{ url('ico/favicon.png') }}">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Пургаз</title>


    <!-- Bootstrap CSS -->
    <link href="{{ url('assets/bootstrap-4.0.0-alpha.6-dist/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ url('assets/font-awesome-4.7.0/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">



    <!-- Styles -->
    <link href="{{ url('css/main.css') }}" rel="stylesheet" type="text/css">

    <!-- Scripts -->
    <script>
        window.Laravel = {
            !!json_encode(['csrfToken' => csrf_token(), ]) !!
        };
    </script>

    <!-- <script src='https://www.google.com/recaptcha/api.js'></script> -->
</head>

<body>
    <!-- Yandex.Metrika counter -->
    <!-- <script type="text/javascript"> (function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter28531321 = new Ya.Metrika({ id:28531321, clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true }); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks"); </script> <noscript><div><img src="https://mc.yandex.ru/watch/28531321" style="position:absolute; left:-9999px;" alt="" /></div></noscript> -->
    <!-- /Yandex.Metrika counter -->


    <div class="container" style="margin-bottom: 1em;">
        <div class="row">
            <div class="col-4">
                <a class="navbar-brand" href="{{ url('/') }}">
                    <img src="{{ url('images/logo.png') }}" alt="logo" id="logo">
                </a>
            </div>
            <div class="col-8">
                <span class="pull-right" style="text-shadow: 2px  2px  5px #696969, 0 0 1px #0093dd; color: white; font-size: 28px; border: 0px solid blue;">
                    <strong>Акционерное общество &laquo;Пургаз&raquo;</strong>
                </span>
            </div>
        </div>
    </div>


    <div class="container">
        @include('navbar')

        @yield('content')
    </div>


    @include('footer')









    <!-- jQuery first, then Tether, then Bootstrap JS. -->
    <script src="{{ url('assets/jquery-3.1.1.min.js') }}"></script>
    <script src="{{ url('assets/tether-master/dist/js/tether.min.js') }}"></script>
    <script src="{{ url('assets/bootstrap-4.0.0-alpha.6-dist/js/bootstrap.min.js') }}"></script>

    <!-- Custom JS -->
    <script src="{{ url('js/purgaz.js') }}"></script>

</body>

</html>