    {{-- <nav class="navbar navbar-inverse bg-inverse navbar-toggleable-md"> --}}
    {{-- <nav class="navbar navbar-light bg-faded rounded navbar-toggleable-md"> --}}
    <nav class="navbar navbar-light bg-faded rounded navbar-toggleable-md" style="background-color: #e3f2fd;">
        <div class="container">
            <button class="navbar-toggler navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleContainer" aria-controls="navbarsExampleContainer" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            {{-- <a class="navbar-brand" href="{{ url('/') }}">Главная</a> --}}


            <div class="collapse navbar-collapse" id="navbarsExampleContainer">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item{{ ends_with(url()->current(), url('about')) ? ' active' : '' }}"><a class="nav-link" href="{{ url('about') }}"><i class="fa fa-info-circle" aria-hidden="true"></i> О компании</a></li>
                    <li class="nav-item{{ ends_with(url()->current(), url('shareholders')) ? ' active' : '' }}"><a class="nav-link" href="{{ url('shareholders') }}"><i class="fa fa-briefcase" aria-hidden="true"></i> Акционерам</a></li>
                    <li class="nav-item{{ ends_with(url()->current(), url('documents')) ? ' active' : '' }}"><a class="nav-link" href="{{ url('documents') }}"><i class="fa fa-file-text-o" aria-hidden="true"></i> Документы</a></li>
                    <li class="nav-item{{ ends_with(url()->current(), url('procurements')) ? ' active' : '' }}"><a class="nav-link" href="{{ url('procurements') }}"><i class="fa fa-rub" aria-hidden="true"></i> Закупки</a></li>
                </ul>

                <ul class="navbar-nav justify-content-end">
                    <li class="nav-item{{ ends_with(url()->current(), url('requisites')) ? ' active' : '' }}"><a class="nav-link" href="{{ url('requisites') }}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Реквизиты</a></li>
                    <li class="nav-item{{ ends_with(url()->current(), url('contacts')) ? ' active' : '' }}"><a class="nav-link" href="{{ url('contacts') }}"><i class="fa fa-envelope-o" aria-hidden="true"></i> Контакты</a></li>
                    @if (!Auth::guest())
                    <li class="nav-item" title="Панель управления"><a class="nav-link text-danger" href="{{ url('adminpanel') }}"><i class="fa fa-cogs" aria-hidden="true"></i></a></li>
                    <li class="nav-item" title="Выход из приложения"><a class="nav-link text-danger" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="fa fa-sign-out" aria-hidden="true"></i></a></li>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                    @endif
                </ul>
            </div>
        </div>
    </nav>