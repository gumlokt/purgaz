@extends("layouts.app")


@section("content")
<div class="row">
    <div class="col">
        <h3>О компании</h3>
    </div>
</div>
<hr>


<div class="row">
    <div class="col">

        <h4 class="text-center">Организация</h4>
        <p class="text-justify">Закрытое акционерное общество «Пургаз» было образовано в 1998 году в городе Губкинский, как совместное предприятие ЗАО «ТЭК «Итера-Русь», входящее в группу компаний «Итера» и ООО «Ноябрьскгаздобыча».
            В мае 2024 года была изменена форма собственности Общества и сейчас полное наименование организации - Акционерное общество «Пургаз».
        </p>

        <h4 class="text-center">История</h4>
        <p class="text-justify">
            <img src="{{ url("images/winter_office.jpg") }}" class="col-xs-6 col-sm-3 img-responsive float-left">
            Проектом обустройства Губкинского газового месторождения было предусмотрено строительство в течение 36 месяцев более 530 объектов, входящих в газодобывающий комплекс. Жесткие сроки, установленные для пуска Губкинского газового промысла, потребовали создания коллектива опытных, грамотных специалистов, способных самостоятельно решать поставленные задачи. Начатое летом 1998 года строительство объектов добычи и подготовки газа, автодорог и ЛЭП было завершено в рекордно короткие сроки – 27 июля 1999 года. Это стало возможным благодаря четкой организации управления, организованным поставкам материально-технических ресурсов и комплектующих, а также эффективной финансово-экономической политике ЗАО «Пургаз» и акционеров.
        </p>

        <h4 class="text-center">Руководство</h4>
        <p class="text-justify">
            С 24 августа 2019 года решением Совета директоров ЗАО «Пургаз» на должность Генерального директора и Председателя Правления Общества избран Стецюкевич Святослав Петрович.
            Стецюкевич Святослав Петрович родился 28 января 1970 года в с. Плоска Острожского района Ровенской области.
            Окончил в 1989 году Славянский энергостроительный техникум по специальности техник.
            В 2005 году Святослав Петрович окончил Тюменский государственный нефтегазовый университет по специальности «Разработка и эксплуатация нефтяных и газовых месторождений», присвоена квалификация инженер. В 2012 году прошел профессиональную переподготовку в Санкт-Петербургском международном институте менеджмента по программе «Мастер Делового Администрирования (МВА)».
            Свою трудовую деятельность Святослав Петрович на Крайнем Севере начал в 1993 году трубопроводчиком линейным 4 разряда Комсомольского газового промысла ноябрьского управления по добыче и транспортировке газа и конденсата предприятия «Сургутгазпром», впоследствии преобразованном в ООО «Ноябрьсгаздобыча» и далее переименованном в ООО «Газпром добыча Ноябрьск» (далее – Общество). На протяжении 20 лет прошел от рабочей профессии трубопроводчика до главного инженера Вынгаяхинского газового промысла Общества (2003г.) и уже в 2004 году был назначен начальником Губкинского газового промысла Общества.
            В 2010 году Святослав Петрович был назначен на должность заместителя генерального директора по общим вопросам ООО «Газпром добыча Ноябрьск» и проработал до избрания его Генеральным директором ЗАО «Пургаз».
        </p>

        <h4 class="text-center">Деятельность</h4>
        <p class="text-justify">
            Основным видом деятельности АО «Пургаз» является добыча природного газа и газового конденсата. На основании лицензии АО «Пургаз» осуществляет добычу сеноманской залежи Губкинского нефтегазоконденсатного месторождения, расположенной на территории Пуровского района, в 25 км северо-восточнее г. Губкинского.
            АО «Пургаз» владеет лицензией на добычу природного газа сеноманской залежи пласта ПК1 Губкинского нефтегазоконденсатного месторождения.
            В перспективных планах деятельности АО «Пургаз» – участие в программах геологического изучения недр Ямало-Ненецкого автономного округа и использование опыта в освоении запасов углеводородов на новых месторождениях региона, а так же решение социальных вопросов г. Губкинского и Пуровского района.
            Закрытым акционерным обществом «Пургаз» производится строительство объектов с учетом современных требований, с применением новейших технологий и материалов, автоматизации процессов, многие из которых применены впервые в суровых климатических условиях Крайнего Севера.
        </p>

        <h4 class="text-center">Участие в жизни города</h4>
        <p class="text-justify">
            <img src="{{ url("images/gubkinskiy.jpg") }}" class="col-xs-6 col-sm-3 img-responsive float-left">
            АО «Пургаз» оказывает финансовую помощь в развитии города Губкинский: строительство подстанций, озеленение города, обустройство дорог, помощь в строительстве храма в городе Губкинском и поселке Пурпе, поддержка творческих коллективов, приобретение игрушек для детских дошкольных учреждений, приобретение оргтехники, оборудования и канцтоваров для школ города, городской библиотеки, – вот далеко не полный перечень благотворительных дел, осуществленных ЗАО «Пургаз».
            Кроме прочего, ЗАО «Пургаз» оказывает благотворительную помощь в виде финансовой поддержки в проведении городских мероприятий, спортивных мероприятий различного уровня с участием спортсменов города, в поддержке социально-реабилитационного центра «Елена», ветеранов войны. Также оказывается помощь организациям: «Ямал-потомкам», Агрофирме «Толькинская» и Территориально-соседской общине «Ича». Выделялись средства для награждения студентов коренной национальности ЯНАО. Неоднократно ЗАО «Пургаз» оказывал помощь в оплате операций и неотложного лечения горожан.
        </p>

        <h4 class="text-center">Награды</h4>
        <p class="text-justify">По итогам деятельности в 2001 г. ЗАО «Пургаз» присуждено звание лауреата 8 Всероссийского конкурса «Карьера – 2001».
            В 2001 году Правительством РФ в рамках Всероссийского конкурса «Российская организация высокой социальной эффективности» ЗАО «Пургаз» вручена грамота за достижения в организации социальной работы.
            Информация о ЗАО «Пургаз» помещена в Золотой книге России. Также Обществу вручен Международный сертификат «Золотой Гермес».
            В 2005 г. Обществу были присуждены диплом «Признание – 2005» и Золотая Версальская медаль.
            Информация о ЗАО «Пургаз» размещена в альманахе «Великая Россия. Эпоха в лицах».
            В 2006 году ЗАО «Пургаз» было присвоен статус «Лидер Российской экономики».
            В 2007 году коллектив Общества награжден грамотой «За доблестный труд во славу Отечества».
            В 2009 году Фонд содействия развития предпринимательства наградил ЗАО «Пургаз» дипломом Международной премии «Элита национальной экономики».
            В апреле 2011 г. Международная Организация Предпринимателей вручила Обществу свидетельство лауреата Всероссийской премии «Предприятие года 2011» и памятный знак.
            В сентябре 2010 г. ЗАО «Пургаз» получил Сертификат «Лидера экономики» за превосходство в отрасли и развитие экономики России.
            10 июля 2013 года ЗАО «Пургаз» стало Почетным членом Фонда поддержки предпринимательских инициатив.
            В 2013 году ЗАО «Пургаз» вручен диплом Лауреата Национальной налоговой премии и присвоено почетное звание «Добросовестный налогоплательщик».
        </p>

        <h4 class="text-center">Награды руководителей</h4>
        <h5>Евко Владимир Павлович <small>(08.03.2001 г. – 23.08.2019 г.)</small></h5>
        <p class="text-justify">
            За период 2005-2010 годы действующему в указанный период времени Генеральному директору ЗАО «Пургаз» Евко Владимиру Павловичу были присвоены почетные звания «Топ-менеджер РФ 2006», «Заслуженный работник нефтяной и газовой промышленности ЯНАО», он был награжден национальной премией «Россиянин года», Медалью Международного ордена Преображения, медалью «За развитие предпринимательства», орденом «Звезда экономики России 3 степени».
        </p>
        <br>

        <h5>Стецюкевич Святослав Петрович</h5>
        <p class="text-justify">
            Трудовая деятельность Святослава Петровича отмечена наградами муниципального (г. Ноябрьск, г. Губкинский) и регионального значения, благодарностью и Почетной грамотой ООО «Газпром добыча Ноябрьск», а также Почетной грамотой Министерства энергетики Российской Федерации.
        </p>
        </p>
    </div>
</div>
@endsection