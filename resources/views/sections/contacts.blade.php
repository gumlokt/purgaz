@extends('layouts.app')


@section('content')
    <div class="row">
        <div class="col">
            <h3>Контакты</h3>
        </div>
    </div>
    <hr>


    <div class="row">
        <div class="col">

                <form action="{{ url('contacts') }}" method="POST">
                    {{ csrf_field() }}

                    <div class="form-group row{{ $errors->has('name') ? ' has-danger' : '' }}">
                        <label for="name" class="col-sm-3 col-form-label"><i class="fa fa-user" aria-hidden="true"></i> Имя</label>
                        <div class="col-sm-9">
                            <input id="name" type="text" class="form-control" name="name" placeholder="Ваше имя" value="{{ old('name') }}" required autofocus>

                            @if ($errors->has('name'))
                                <div class="form-control-feedback">
                                    {!! $errors->first('name') !!}
                                </div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row{{ $errors->has('email') ? ' has-danger' : '' }}">
                        <label for="email" class="col-sm-3 col-form-label"><i class="fa fa-at" aria-hidden="true"></i> E-Mail</label>
                        <div class="col-sm-9">
                            <input id="email" type="email" class="form-control" name="email" placeholder="Ваш E-Mail" value="{{ old('email') }}" required>

                            @if ($errors->has('email'))
                                <div class="form-control-feedback">
                                    {!! $errors->first('email') !!}
                                </div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row{{ $errors->has('subject') ? ' has-danger' : '' }}">
                        <label for="subject" class="col-sm-3 col-form-label"><i class="fa fa-header" aria-hidden="true"></i> Тема</label>
                        <div class="col-sm-9">
                            <input id="subject" type="text" class="form-control" name="subject" placeholder="Тема сообщения" value="{{ old('subject') }}" required>

                            @if ($errors->has('subject'))
                                <div class="form-control-feedback">
                                    {!! $errors->first('subject') !!}
                                </div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row{{ $errors->has('message') ? ' has-danger' : '' }}">
                        <label for="message" class="col-sm-3 col-form-label"><i class="fa fa-file-text-o" aria-hidden="true"></i> Сообщение</label>
                        <div class="col-sm-9">
                            <textarea id="message" class="form-control" name="message" rows='5'>{{ old('message') }}</textarea>

                            @if ($errors->has('message'))
                                <div class="form-control-feedback">
                                    {!! $errors->first('message') !!}
                                </div>
                            @endif
                        </div>
                    </div>



                    <div class="form-group row">
                        <label for="CaptchaCode" class="col-sm-3 col-form-label"><i class="fa fa-question-circle-o" aria-hidden="true"></i> Капча</label>
                        <div class="col-sm-9">
                            {!! Captcha::display() !!}
                        </div>
                    </div>


                    <div class="form-group row">
                        <div class="offset-sm-3 col-sm-9">
                            <div class="text-muted">
                                <em>
                                    <span class="text-danger">*</span>
                                    <small>Все поля обязательны для заполнения</small>
                                </em>
                            </div>
                        </div>
                    </div>


                    <div class="form-group row">
                        <div class="offset-sm-3 col-sm-2">
                            <button type="submit" class="btn btn-secondary"><i class="fa fa-envelope-o" aria-hidden="true"></i> Отправить</button>
                        </div>
                        <div class="col-sm-7">
                            @if (session('message'))
                                <div class="alert alert-success alert-dismissible fade show fadingMessage" role="alert" style="margin: 0; padding: 5px 8px;">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close" style="margin: 0; padding: 8px 16px 0px;">
                                        <span aria-hidden="true">&times;</span>
                                    </button>

                                    {{ session('message') }}
                                </div>
                            @endif
                        </div>
                    </div>

                </form>





        </div>
    </div>
@endsection
