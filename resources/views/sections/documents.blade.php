@extends("layouts.app")


@section("content")
<div class="row">
    <div class="col">
        <h3>Документы</h3>
    </div>
</div>
<hr>


<div class="row">
    <div class="col">

        <h5 class="text-danger">Промышленная безопасность</h5>
        <table class="table table-hover table-striped">
            <tr>
                <td>
                    Политика АО "Пургаз" в области промышленной безопасности
                </td>
                <td width="17%">
                    <a href="{{ url('files/documents/policy_statement.pdf') }}" target="_blank">
                        <i class="fa fa-file-pdf-o text-danger" aria-hidden="true"></i> Скачать
                    </a>
                </td>
            </tr>
            <tr>
                <td>
                    Политика АО "Пургаз" в области охраны труда
                </td>
                <td width="17%">
                    <a href="{{ url('files/documents/policy_labor_protection.pdf') }}" target="_blank">
                        <i class="fa fa-file-pdf-o text-danger" aria-hidden="true"></i> Скачать
                    </a>
                </td>
            </tr>
        </table>
        <br>


        <h5 class="text-danger">Условия труда</h5>
        <table class="table table-hover table-striped">
            <tr>
                <td>
                    Сводная ведомость результатов проведения специальной оценки условий труда 2020
                </td>
                <td width="17%">
                    <a href="{{ url('files/documents/svodnaya_vedomost_2020.pdf') }}" target="_blank">
                        <i class="fa fa-file-pdf-o text-danger" aria-hidden="true"></i> Скачать
                    </a>
                </td>
            </tr>
            <tr>
                <td>
                    Перечень рекомендуемых мероприятий по улучшению условий труда 2020
                </td>
                <td width="17%">
                    <a href="{{ url('files/documents/perechen_meropriyatiy_2020.pdf') }}" target="_blank">
                        <i class="fa fa-file-pdf-o text-danger" aria-hidden="true"></i> Скачать
                    </a>
                </td>
            </tr>
            <tr>
                <td>
                    Сводная ведомость результатов проведения специальной оценки условий труда 2022
                </td>
                <td width="17%">
                    <a href="{{ url('files/documents/svodnaya_vedomost_2022.pdf') }}" target="_blank">
                        <i class="fa fa-file-pdf-o text-danger" aria-hidden="true"></i> Скачать
                    </a>
                </td>
            </tr>
            <tr>
                <td>
                    Перечень рекомендуемых мероприятий по улучшению условий труда 2022
                </td>
                <td width="17%">
                    <a href="{{ url('files/documents/perechen_meropriyatiy_2022.pdf') }}" target="_blank">
                        <i class="fa fa-file-pdf-o text-danger" aria-hidden="true"></i> Скачать
                    </a>
                </td>
            </tr>
            <tr>
                <td>
                    Сводная ведомость результатов проведения специальной оценки условий труда 2023
                </td>
                <td width="17%">
                    <a href="{{ url('files/documents/svodnaya_vedomost_2023.pdf') }}" target="_blank">
                        <i class="fa fa-file-pdf-o text-danger" aria-hidden="true"></i> Скачать
                    </a>
                </td>
            </tr>

            <tr>
                <td>
                    Перечень рекомендуемых мероприятий по улучшению условий труда 2023
                </td>
                <td width="17%">
                    <a href="{{ url('files/documents/perechen_meropriyatiy_2023.pdf') }}" target="_blank">
                        <i class="fa fa-file-pdf-o text-danger" aria-hidden="true"></i> Скачать
                    </a>
                </td>
            </tr>

        </table>
        <br>


        <h5 class="text-danger">Информация по аккредитации</h5>
        <table class="table table-hover table-striped">
            <tr>
                <td>
                    Инструкция прохождения аккредитации лицами, претендующими на участие в закупочных процедурах АО "Пургаз"
                </td>
                <td width="17%">
                    <a href="{{ url('files/accreditation/instrukciya_po_akkreditacii.pdf') }}" target="_blank">
                        <i class="fa fa-file-pdf-o text-danger" aria-hidden="true"></i> Скачать
                    </a>
                </td>
            </tr>
            <tr>
                <td>
                    Приложение 1 к инструкции по аккредитации
                </td>
                <td width="17%">
                    <a href="{{ url('files/accreditation/prilozhenie_01.docx') }}" target="_blank">
                        <i class="fa fa-file-word-o text-primary" aria-hidden="true"></i> Скачать
                    </a>
                </td>
            </tr>
            <tr>
                <td>
                    Приложение 2 к инструкции по аккредитации
                </td>
                <td width="17%">
                    <a href="{{ url('files/accreditation/prilozhenie_02.docx') }}" target="_blank">
                        <i class="fa fa-file-word-o text-primary" aria-hidden="true"></i> Скачать
                    </a>
                </td>
            </tr>
            <tr>
                <td>
                    Приложение 3 к инструкции по аккредитации
                </td>
                <td width="17%">
                    <a href="{{ url('files/accreditation/prilozhenie_03.pdf') }}" target="_blank">
                        <i class="fa fa-file-pdf-o text-danger" aria-hidden="true"></i> Скачать
                    </a>
                </td>
            </tr>
            <tr>
                <td>
                    Приложение 4 к инструкции по аккредитации
                </td>
                <td width="17%">
                    <a href="{{ url('files/accreditation/prilozhenie_04.docx') }}" target="_blank">
                        <i class="fa fa-file-word-o text-primary" aria-hidden="true"></i> Скачать
                    </a>
                </td>
            </tr>
            <tr>
                <td>
                    Приложение 5 к инструкции по аккредитации
                </td>
                <td width="17%">
                    <a href="{{ url('files/accreditation/prilozhenie_05.docx') }}" target="_blank">
                        <i class="fa fa-file-word-o text-primary" aria-hidden="true"></i> Скачать
                    </a>
                </td>
            </tr>
            <tr>
                <td>
                    Контакты по вопросам подготовки документов для аккредитации
                </td>
                <td width="17%">
                    <a href="{{ url('files/accreditation/kontakty_po_akkreditacii.docx') }}">
                        <i class="fa fa-file-word-o text-primary" aria-hidden="true"></i> Скачать
                    </a>
                </td>
            </tr>
        </table>
        <br>


        <h5 class="text-danger">Материалы для проведения публичных слушаний</h5>
        <table class="table table-hover table-striped">
            <tr>
                <td>
                    Общественные обсуждения проектной документации «Рекультивация полигона твердых бытовых отходов Губкинского газового месторождения», включая предварительные материалы оценки воздействия на окружающую среду
                </td>
                <td width="17%">
                    <a href="{{ url('rekultivaciya') }}">
                        <i class="fa fa-long-arrow-right text-danger" aria-hidden="true"></i> Открыть
                    </a>
                </td>
            </tr>
        </table>
        <br>


        <h5 class="text-danger">Государственная экологическая экспертиза</h5>
        <table class="table table-hover table-striped">
            <tr>
                <td>
                    Рекультивация полигона твердых бытовых отходов Губкинского газового месторождения
                </td>
                <td width="17%">
                    <a href="{{ url('files/documents/documentaciya.zip') }}" target="_blank">
                        <i class="fa fa-file-archive-o text-danger" aria-hidden="true"></i> Скачать <small>(~192 Мб)</small>
                    </a>
                    <!-- <br>
                    <a href="{{ url('files/documents/proektnaya_documentatsia.zip.sig') }}" target="_blank">
                        <i class="fa fa-certificate text-primary" aria-hidden="true"></i> Скачать <small>(~5 Кб)</small>
                    </a> -->
                </td>
            </tr>
        </table>
        <br>






    </div>
</div>
@endsection