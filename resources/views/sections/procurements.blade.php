@extends('layouts.app')


@section('content')
    <div class="row">
        <div class="col">
            <h3>Закупки</h3>
        </div>
    </div>
    <hr>


    <div class="row">
        <div class="col">

            @if (isset($archives))
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#actual" role="tab">Действующие</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#archives" role="tab">Архивные</a>
                    </li>
                </ul>

                <div class="tab-content">
                    <div class="tab-pane active" id="actual" role="tabpanel">
            @endif

                        @if (isset($procurements) and !empty($procurements[0]))
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th width="11%">№ закупки</th>
                                        <th>Наименование</th>

                                        @if ('show' == $settings->comments)
                                            <th>Примечание</th>
                                        @endif

                                        <th width="15%">Файлы</th>
                                        <th>Начало</th>
                                        <th>Окончание</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($procurements as $procurement)
                                        <tr class="procurement">
                                            <td>{{ $procurement->number }}</td>
                                            <td>{{ $procurement->title }}</td>
                                            @if ('show' == $settings->comments)
                                                <td>{{ $procurement->comment }}</td>
                                            @endif

                                            <td>
                                                @if ($procurement->announcement)
                                                    <i class="fa fa-file-pdf-o text-danger" aria-hidden="true"></i> <a href="{{ url('download/announcement/' . $procurement->id) }}">Извещение</a>
                                                    <br>
                                                @endif

                                                @if ($procurement->documentation)
                                                    <i class="fa fa-file-archive-o text-danger" aria-hidden="true"></i> <a href="{{ url('download/documentation/' . $procurement->id) }}">Документация</a>
                                                @endif
                                            </td>

                                            <td>{{ $procurement->dateStart }}</td>
                                            <td>{{ $procurement->dateEnd }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        @else
                            <div class="text-center">Нет данных для отображения</div>
                        @endif

            @if (isset($archives))
                    </div>
                    <div class="tab-pane" id="archives" role="tabpanel">
                        @if (!empty($archives[0]))
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th width="11%">№ закупки</th>
                                        <th>Наименование</th>

                                        @if ('show' == $settings->comments)
                                            <th>Примечание</th>
                                        @endif

                                        <th width="15%">Файлы</th>
                                        <th>Начало</th>
                                        <th>Окончание</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($archives as $archive)
                                        <tr class="archive">
                                            <td>{{ $archive->number }}</td>
                                            <td>{{ $archive->title }}</td>
                                            @if ('show' == $settings->comments)
                                                <td>{{ $archive->comment }}</td>
                                            @endif

                                            <td>
                                                @if ($archive->announcement)
                                                    <i class="fa fa-file-pdf-o text-danger" aria-hidden="true"></i> <a href="{{ url('download/announcement/' . $archive->id) }}">Извещение</a>
                                                    <br>
                                                @endif

                                                @if ($archive->documentation)
                                                    <i class="fa fa-file-archive-o text-danger" aria-hidden="true"></i> <a href="{{ url('download/documentation/' . $archive->id) }}">Документация</a>
                                                @endif
                                            </td>

                                            <td>{{ $archive->dateStart }}</td>
                                            <td>{{ $archive->dateEnd }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        @else
                            <div class="text-center">Нет данных для отображения</div>
                        @endif
                    </div>
                </div>
            @endif


        </div>
    </div>
@endsection
