@extends("layouts.app")


@section("content")
<div class="row">
    <div class="col">
        <h3>Материалы для проведения публичных слушаний</h3>
    </div>
</div>
<hr>


<div class="row">
    <div class="col">
        <p class="text-justify" style="box-sizing: border-box; margin: 0; padding: 0 0 5px;">
            Общественные обсуждения проектной документации «Рекультивация полигона твердых бытовых отходов Губкинского газового месторождения», включая предварительные материалы оценки воздействия на окружающую среду.
        </p>
        <p class="text-justify font-italic font-weight-light">
            Дата публикации: 10.06.2024 г.
        </p>
        <p class="text-justify">
            <strong>Материалы для скачивания:</strong>
        </p>
        <table class="table table-hover table-striped">
            <tr>
                <td>
                    Документация
                </td>
                <td width="10%">
                    <a href="{{ url('files/documents/documentaciya.zip') }}" target="_blank">
                        <i class="fa fa-download text-danger" aria-hidden="true"></i> Скачать
                    </a>
                </td>
            </tr>
        </table>
    </div>
</div>
@endsection