@extends("layouts.app")


@section("content")
<div class="row">
    <div class="col">
        <h3>Реквизиты</h3>
    </div>
</div>
<hr>


<div class="row">
    <div class="col">

        <table class="table table-sm table-hover">
            <tr>
                <td>Полное наименование:</td>
                <td><strong>Акционерное общество &laquo;Пургаз&raquo;</strong></td>
            </tr>
            <tr>
                <td>Сокращенное наименование:</td>
                <td><strong>АО &laquo;Пургаз&raquo;</strong></td>
            </tr>
            <tr>
                <td>Юридический и почтовый адреса:</td>
                <td>
                    <strong>
                        Российская Федерация, 629831, Ямало-Ненецкий автономный округ,<br>
                        город Губкинский, микрорайон 16, дом 52
                    </strong>
                </td>
            </tr>
            <tr>
                <td>Электронная почта:</td>
                <td><strong>aopurgaz@purgaz.com</strong></td>
            </tr>
            <tr>
                <td>Телефон:</td>
                <td><strong>(34936) 49-360, 49-375</strong><br></td>
            </tr>
            <tr>
                <td>Факс:</td>
                <td><strong>(34936) 49-340</strong></td>
            </tr>
            <tr>
                <td>ИНН:</td>
                <td><strong>89 13 000 816</strong></td>
            </tr>
            <tr>
                <td>КПП:</td>
                <td><strong>54 605 0001</strong></td>
            </tr>
            <tr>
                <td>ОКОПФ:</td>
                <td><strong>122 67</strong></td>
            </tr>
            <tr>
                <td>ОКПО:</td>
                <td><strong>47 85 62 57</strong></td>
            </tr>
            <tr>
                <td>ОКОГУ:</td>
                <td><strong>421 00 14</strong></td>
            </tr>
            <tr>
                <td>ОКТМО:</td>
                <td><strong>71 952 000</strong></td>
            </tr>
            <tr>
                <td>ОКВЭД:</td>
                <td><strong>06.20</strong></td>
            </tr>
            <tr>
                <td>ОКФС:</td>
                <td><strong>16</strong></td>
            </tr>
            <tr>
                <td>ОГРН:</td>
                <td><strong>102 890 089 75 74</strong></td>
            </tr>
            <tr>
                <td>Банк:</td>
                <td>
                    <strong>
                        Банк ГПБ (АО), г. Москва
                    </strong>
                    <br>
                </td>
            </tr>
            <tr>
                <td>Расчетный счет:</td>
                <td><strong>40 702 810 400 000 072 451</strong></td>
            </tr>
            <tr>
                <td>Корсчет:</td>
                <td><strong>30 101 810 200 000 000 823</strong></td>
            </tr>
            <tr>
                <td>БИК:</td>
                <td><strong>044 525 823</strong></td>
            </tr>
            <tr>
                <td>Генеральный директор:</td>
                <td><strong>Стецюкевич Святослав Петрович</strong></td>
            </tr>
            <tr>
                <td>Финансовый директор:</td>
                <td><strong>Лутай Сергей Евгеньевич</strong></td>
            </tr>
            <tr>
                <td>Главный бухгалтер:</td>
                <td><strong>Бочкарева Елена Валентиновна</strong></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
            </tr>
        </table>

        <p class="lead">
            Скачать реквизиты: <a href="{{ url("files/requisites/purgaz_requisites.xlsx") }}"><i class="fa fa-file-excel-o fa-lg text-success" aria-hidden="true"></i></a>
        </p>
    </div>
</div>
@endsection