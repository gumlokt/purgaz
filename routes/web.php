<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::any('adminpanel', 'AdminController@index');
Route::any('home', 'AdminController@index');

Route::any('adminpanel/procurments/{status}', 'AdminController@show')->name('procurements');


// Actions with procurement
Route::get('adminpanel/procurment/create', 'ProcurementController@create');
Route::post('adminpanel/procurment/store', 'ProcurementController@store');

Route::get('adminpanel/procurment/{id}/show', 'ProcurementController@show');
Route::any('adminpanel/procurment/{id}/edit', 'ProcurementController@edit');
Route::post('adminpanel/procurment/{id}/update', 'ProcurementController@update');

Route::get('adminpanel/procurment/{id}/activate', 'ProcurementController@activate');
Route::get('adminpanel/procurment/{id}/draft', 'ProcurementController@draft');
Route::get('adminpanel/procurment/{id}/archive', 'ProcurementController@archive');

// Route::get('adminpanel/procurment/{id}/destroy', 'ProcurementController@destroy');
// Route::post('adminpanel/procurment/{id}/destroy', 'ProcurementController@destroy');
Route::any('adminpanel/procurment/{id}/destroy', 'ProcurementController@destroy');

// Route::get('adminpanel/archives/destroyall', 'ProcurementController@destroyAll');
// Route::post('adminpanel/archives/destroyall', 'ProcurementController@destroyAll');
Route::any('adminpanel/archives/destroyall', 'ProcurementController@destroyAll');


// download and delete files
Route::get('adminpanel/{action}/{file}/{id}', 'ProcurementController@file');


// Web-application settings
Route::get('adminpanel/settings/show', 'SettingController@show');
Route::post('adminpanel/settings/save', 'SettingController@save');
Route::post('adminpanel/settings/cancel', 'SettingController@cancel');





// download files
Route::get('download/{type}/{id}', 'FileController@file');



Route::get('procurements', 'IndexController@procurements');
Route::get('contacts', 'IndexController@contacts');
Route::post('contacts', 'IndexController@contacts');

// block new registrations
Route::any('register', function () { return redirect(''); });

// default action
Route::get('{section?}', 'IndexController@section');

